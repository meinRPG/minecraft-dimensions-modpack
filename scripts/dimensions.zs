print("Injecting Dimensions Recipes ...");
// Some often used items
var planks = <ore:plankWood>;
var wood = <ore:logWood>;
var slabWood = <ore:slabWood>;
var stairWood = <ore:stairWood>;
var leaves = <ore:treeLeaves>;
var sapling = <ore:treeSapling>;
var stick = <minecraft:stick>;
var cobble = <minecraft:cobblestone>;
var iron = <minecraft:iron_ingot>;
var gold = <minecraft:gold_ingot>;
var diamond = <minecraft:diamond>;
var emerald = <minecraft:emerald>;
var thaumium = <Thaumcraft:ItemResource:2>;
var bronze = <TConstruct:materials:13>;
var mithril = <customnpcs:npcMithrilIngot>;
var mana = <customnpcs:npcMana>;
var holystone = <aether:holystone:1>;
// ==========================
// ==== Hide Stuff
// ==========================
// NPC Bronze - We use the Tinker's bronze.
//NEI.hide(<customnpcs:npcBronzeIngot>);
// ==========================
// ==== Blocks
// ==========================
// Carpentry Workbench
recipes.addShaped(<customnpcs:npcCarpentyBench>,
 [[planks, <TConstruct:CraftingStation:*>, planks],
  [stick, stick, stick]]);
// Holystone -> Stone
recipes.addShaped(<minecraft:stone>,
 [[holystone, holystone, holystone],
  [holystone, holystone, holystone],
  [holystone, holystone, holystone]]);
// Wood + Mana -> Magic Wood
recipes.addShapeless(<BiomesOPlenty:logs2:1> * 8, [mana, wood, wood, wood, wood, wood, wood, wood, wood]);
recipes.addShapeless(<BiomesOPlenty:leaves1:2> * 8, [mana, leaves, leaves, leaves, leaves, leaves, leaves, leaves, leaves]);
recipes.addShapeless(<BiomesOPlenty:saplings:3> * 8, [mana, <minecraft:sapling>, <minecraft:sapling>, <minecraft:sapling>, <minecraft:sapling>, <minecraft:sapling>, <minecraft:sapling>, <minecraft:sapling>, <minecraft:sapling>]);
recipes.addShapeless(<BiomesOPlenty:planks:5> * 8, [mana, planks, planks, planks, planks, planks, planks, planks, planks]);
recipes.addShapeless(<BiomesOPlenty:woodenSingleSlab1:5> * 8, [mana, slabWood, slabWood, slabWood, slabWood, slabWood, slabWood, slabWood, slabWood]);
recipes.addShapeless(<BiomesOPlenty:magicStairs> * 8, [mana, stairWood, stairWood, stairWood, stairWood, stairWood, stairWood, stairWood, stairWood]);
// The Aether II map generator has a bug which spawns Clouds with a damage of 5. These blocks do not actually exist.
// Same goes for logs with damage of 1.
recipes.addShapeless(<aether:aercloud:0>, [<aether:aercloud:5>]);
recipes.addShapeless(<aether:aetherLog:0>, [<aether:aetherLog:1>]);
// Converting all clouds
recipes.addShapeless(<aether:aercloud:0> * 2, [<TwilightForest:tile.FluffyCloud>, <TwilightForest:tile.FluffyCloud>]);
recipes.addShapeless(<TwilightForest:tile.WispyCloud> * 2, [<aether:aercloud:0>, <aether:aercloud:0>]);
recipes.addShapeless(<chisel:cloud:0> * 2, [<TwilightForest:tile.WispyCloud>, <TwilightForest:tile.WispyCloud>]);
recipes.addShapeless(<TwilightForest:tile.FluffyCloud> * 2, [<chisel:cloud:*>, <chisel:cloud:*>]);
// ==========================
// ==== Items
// ==========================
// Mana
recipes.addShapeless(mana, [<minecraft:redstone>, <minecraft:glowstone_dust>]);
// Mithril
recipes.addShapeless(mithril, [iron, mana]);
// Wooden Glaive
recipes.addShaped(<customnpcs:npcWoodenGlaive>,
 [[planks, null, null],
  [null, stick, null],
  [null, null, planks]]);
// Stone Glaive
recipes.addShaped(<customnpcs:npcStoneGlaive>,
 [[cobble, null, null],
  [null, stick, null],
  [null, null, cobble]]);
// Frost Glaive
//recipes.addShaped(<customnpcs:npcFrostGlaive>,
// [[<meteors:FrezaCrystal>, null, null],
//  [null, stick, null],
//  [null, null, <meteors:FrezaCrystal>]]);
// Iron Glaive
recipes.addShaped(<customnpcs:npcIronGlaive>,
 [[iron, null, null],
  [null, stick, null],
  [null, null, iron]]);
// Diamond Glaive
recipes.addShaped(<customnpcs:npcDiamondGlaive>,
 [[diamond, null, null],
  [null, stick, null],
  [null, null, diamond]]);
// Demonic Glaive
recipes.addShaped(<customnpcs:npcDemonicGlaive>,
 [[thaumium, null, null],
  [null, stick, null],
  [null, null, thaumium]]);
// Mithril Glaive
recipes.addShaped(<customnpcs:npcMithrilGlaive>,
 [[mithril, null, null],
  [null, stick, null],
  [null, null, mithril]]);
// Emerald Glaive
recipes.addShaped(<customnpcs:npcEmeraldGlaive>,
 [[emerald, null, null],
  [null, stick, null],
  [null, null, emerald]]);
// Golden Glaive
recipes.addShaped(<customnpcs:npcGoldenGlaive>,
 [[gold, null, null],
  [null, stick, null],
  [null, null, gold]]);
print("... Done Injecting Dimensions Recipes");